# aws-k8-cluster

A CloudFormation template and AMI builder for running Kubernetes on AWS. The AMI is built with Packer and includes the 
Kubernetes packages for installation. The template launches all the resources needed to create cluster on AWS including 
VPC, Launch Configs, RDS database etc.


## Design

The AMI is built with [Packer](https://www.packer.io/) and [Ansible](https://www.ansible.com/community) is used to 
install Kubernetes packages.

The CloudFormation template creates the following key components:

- Virtual Private Cloud
- K8 master EC2 instance
- Auto Scaling Group for the K8 nodes
- Security Group for K8 & pod networks
- Postgres RDS instance as DB backend


## Usage

### Packer and Ansible

[Packer](https://www.packer.io/) uses a base ubuntu 16.04 Xenial image to launch an instance, installs Kubernetes via 
[Ansible](https://www.ansible.com/community) and creates a custom image. 

Please export the following variables for packer to authenticate to AWS in order to create a custom K8 AMI from the 
Ubuntu 16.0.4 xenial base image. The base AMI id can be found on AWS. They are specific for different regions. 

If you are on a MAC and use homebrew, please install packer.

    $ brew install packer
    
    $ export AWS_ACCESS_KEY_ID="anaccesskey"
    $ export AWS_SECRET_ACCESS_KEY="asecretkey"
    $ export AWS_DEFAULT_REGION="eu-central-1"
    $ export AWS_SOURCE_AMI="ami-086a09d5b9fa35dc7"
    
    $ packer validate -var source_ami=$AWS_SOURCE_AMI packer/ami.json
    $ packer build -var source_ami=$AWS_SOURCE_AMI packer/ami.json
    
Please take a note of the AMI ID created by Packer. This will be used as cloudformation parameter when you create the 
cluster.


### Cloudformation

Create a SSH key in the region you would like to create your K8 cluster. Please click on 
[Launch Stack](https://console.aws.amazon.com/cloudformation/home#/stacks/new) and create a new stack.
The cloudformation template can be found in the AWS directory in the project. Please add in all the required parameters. 
Each parameter has a description and a default value.

Once the cluster is successfully created, the template outputs Cluster and Database settings. Please make a note of this.
See example below.

        GetKubeconfigCommand: scp -i k8.pem ubuntu@3.121.198.130:.kube/config ./kubeconfig		
        DatabasePort: 3306		
        LoginToMasterCommand: ssh -i k8.pem ubuntu@3.121.198.130		
        DatabaseHostname: database-kube8.aakkaaa.eu-central-1.rds.amazonaws.com		
        MasterIP: 3.121.198.130
        
The nodes are in a AutoScaling group. If any node instances gets terminated a new one will be created by auto scaling
and it will automatically join the cluster. The current default is 3 nodes but can be increased by updating the Cloudformation
nodes parameter.


### Kubernetes

You can now connect to the Kubernetes cluster. If you are on a MAC and use homebrew, please install the k8-cli.

        $ brew install kubernetes-cli
        $ scp -i k8.pem ubuntu@3.121.198.130:.kube/config ./kubeconfig
        

Alternatively, you can ssh on to the master and run kube commands from there.


        $ ssh -i k8.pem ubuntu@3.121.198.130
        
        $ kubectl get nodes
        NAME              STATUS   ROLES    AGE    VERSION
        ip-10-120-1-51    Ready    master   108m   v1.12.2
        ip-10-120-1-68    Ready    <none>   108m   v1.12.2
        ip-10-120-2-134   Ready    <none>   108m   v1.12.2
        ip-10-120-3-76    Ready    <none>   108m   v1.12.2
        

### Deploying Applications

You are now ready to deploy your first application on the k8 cluster on AWS. A sample app can be found in the k8_apps
directory. The webapp1 contains a deployment.yml file and service.yml.

[Deployments](https://kubernetes.io/docs/concepts/workloads/controllers/deployment/) are used to create pods and 
replica sets. In the webapp1 example we are creating two replica sets of our app. You can scale the app by updating the
replica set value. 

        $ kubectl create -f k8_apps/webapp1/deployment.yml
        
        $ kubectl describe deployments
        Name:                   webapp1
        Namespace:              default
        CreationTimestamp:      Sun, 18 Nov 2018 19:53:19 +0000
        Labels:                 app=webapp1
        Annotations:            deployment.kubernetes.io/revision: 1
        Selector:               app=webapp1
        Replicas:               2 desired | 2 updated | 2 total | 2 available | 0 unavailable
        StrategyType:           RollingUpdate
        MinReadySeconds:        0
        RollingUpdateStrategy:  1 max unavailable, 1 max surge
        Pod Template:
          Labels:  app=webapp1
          Containers:
           webapp1:
            Image:        katacoda/docker-http-server:latest
            Port:         80/TCP
            Host Port:    0/TCP
            Environment:  <none>
            Mounts:       <none>
          Volumes:        <none>
        Conditions:
          Type           Status  Reason
          ----           ------  ------
          Available      True    MinimumReplicasAvailable
        OldReplicaSets:  <none>
        NewReplicaSet:   webapp1-95bf7b6f8 (2/2 replicas created)
        Events:          <none>
        

[Service](https://kubernetes.io/docs/concepts/services-networking/service/) is an abstraction which defines a logical 
set of Pods and a policy by which to access them - sometimes called a micro-service. In the Webapp1 example we are 
creating a service for our webapp1 and exposing port 30080 so that our app is accessible externally.
    
        $ kubectl create -f k8_apps/webapp1/service.yml
        
        $ kubectl describe service
        Name:              kubernetes
        Namespace:         default
        Labels:            component=apiserver
                           provider=kubernetes
        Annotations:       <none>
        Selector:          <none>
        Type:              ClusterIP
        IP:                10.96.0.1
        Port:              https  443/TCP
        TargetPort:        6443/TCP
        Endpoints:         3.121.198.130:6443
        Session Affinity:  None
        Events:            <none>
        Name:                     webapp1-svc
        Namespace:                default
        Labels:                   app=webapp1
        Annotations:              <none>
        Selector:                 app=webapp1
        Type:                     NodePort
        IP:                       10.99.181.229
        Port:                     <unset>  80/TCP
        TargetPort:               80/TCP
        NodePort:                 <unset>  30080/TCP
        Endpoints:                10.32.0.2:80,10.44.0.3:80
        Session Affinity:         None
        External Traffic Policy:  Cluster
        Events:                   <none>
        
        
### Accessing the application

Amazon does not assign a public IP to an instance directly. They use 1-to-1 NAT. Hence the public ip is not directly
exposed on the instance even though it's launched in a public subnet and routable on the internet. K8 is unaware of this
address. Please fine the public ip assigned to any of your node instances via the console or the cli and browse to the
address on port 30080

            $ curl http://3.121.116.159:30080/
            <h1>This request was processed by host: webapp1-95bf7b6f8-ztz4s</h1>
           
           

## TODO

##### Use AWS Elastic Load Balancer instead of going directly to node IP's.
##### Make instances private instead of public facing and use ELB as a reverse proxy.
##### Add Cloudwatch alarms to Autoscale nodes based on CPU/memory Usage. Currently you have to update the nodes CF parameter.
##### Orchestrate it in gitlab-ci/jenkins/other build/deploy pipeline.
##### The sample webapp1 does not currently utilise RDS, Even though it can connect to it. Update the app and use K8 secrets for passing credentials. 